﻿/*
 * Created by SharpDevelop.
 * User: aarond
 * Date: 3/17/2012
 * Time: 11:56 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace GreenshotFogBugzPlugin
{
	partial class SendForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendForm));
            this.GoButton = new System.Windows.Forms.Button();
            this.newCaseButton = new System.Windows.Forms.RadioButton();
            this.existingCaseButton = new System.Windows.Forms.RadioButton();
            this.onDemandName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.caseNumberUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.caseNumberUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // GoButton
            // 
            this.GoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GoButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.GoButton.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GoButton.Location = new System.Drawing.Point(28, 133);
            this.GoButton.Margin = new System.Windows.Forms.Padding(4);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(389, 53);
            this.GoButton.TabIndex = 0;
            this.GoButton.Text = "Create new case";
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButtonClick);
            // 
            // newCaseButton
            // 
            this.newCaseButton.AutoSize = true;
            this.newCaseButton.Checked = true;
            this.newCaseButton.Location = new System.Drawing.Point(28, 60);
            this.newCaseButton.Margin = new System.Windows.Forms.Padding(4);
            this.newCaseButton.Name = "newCaseButton";
            this.newCaseButton.Size = new System.Drawing.Size(93, 23);
            this.newCaseButton.TabIndex = 5;
            this.newCaseButton.TabStop = true;
            this.newCaseButton.Text = "New case";
            this.newCaseButton.UseVisualStyleBackColor = true;
            // 
            // existingCaseButton
            // 
            this.existingCaseButton.AutoSize = true;
            this.existingCaseButton.Location = new System.Drawing.Point(28, 91);
            this.existingCaseButton.Margin = new System.Windows.Forms.Padding(4);
            this.existingCaseButton.Name = "existingCaseButton";
            this.existingCaseButton.Size = new System.Drawing.Size(117, 23);
            this.existingCaseButton.TabIndex = 6;
            this.existingCaseButton.Text = "Existing case";
            this.existingCaseButton.UseVisualStyleBackColor = true;
            this.existingCaseButton.CheckedChanged += new System.EventHandler(this.existingCaseButton_CheckedChanged);
            // 
            // onDemandName
            // 
            this.onDemandName.Location = new System.Drawing.Point(92, 26);
            this.onDemandName.Name = "onDemandName";
            this.onDemandName.Size = new System.Drawing.Size(188, 27);
            this.onDemandName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label2.Location = new System.Drawing.Point(24, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "https://";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label3.Location = new System.Drawing.Point(286, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = ".fogbugz.com";
            // 
            // caseNumberUpDown
            // 
            this.caseNumberUpDown.Enabled = false;
            this.caseNumberUpDown.Location = new System.Drawing.Point(154, 91);
            this.caseNumberUpDown.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.caseNumberUpDown.Name = "caseNumberUpDown";
            this.caseNumberUpDown.Size = new System.Drawing.Size(126, 27);
            this.caseNumberUpDown.TabIndex = 7;
            this.caseNumberUpDown.TextChanged += new System.EventHandler(this.caseNumberUpDown_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label1.Location = new System.Drawing.Point(25, 202);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tip: Use Ctrl+F next time to save even more time. ";
            // 
            // SendForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(444, 225);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.caseNumberUpDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.onDemandName);
            this.Controls.Add(this.existingCaseButton);
            this.Controls.Add(this.newCaseButton);
            this.Controls.Add(this.GoButton);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SendForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Send to FogBugz";
            this.Load += new System.EventHandler(this.SendFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.caseNumberUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.RadioButton newCaseButton;
        private System.Windows.Forms.RadioButton existingCaseButton;
        private System.Windows.Forms.TextBox onDemandName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown caseNumberUpDown;
        private System.Windows.Forms.Label label1;
	}
}
