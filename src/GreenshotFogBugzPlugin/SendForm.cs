﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Greenshot.IniFile;

namespace GreenshotFogBugzPlugin
{
    public partial class SendForm : GreenshotPlugin.Controls.GreenshotForm
    {
        readonly FogBugzConfiguration configuration;
        readonly MemoryStream captureStream;

        public SendForm(FogBugzConfiguration configuration, MemoryStream captureStream)
        {
            InitializeComponent();

            this.configuration = configuration;
            this.captureStream = captureStream;
        }

        void SendFormLoad(object sender, EventArgs e)
        {
            // If we have it already - get the URL from the configuration. We'll save it on submit.
            if (!string.IsNullOrEmpty(configuration.FogBugzOnDemandName) &&
                onDemandName.Text != configuration.FogBugzOnDemandName)
            {
                onDemandName.Text = configuration.FogBugzOnDemandName;
            }
            caseNumberUpDown.Value = configuration.FogBugsLastCaseNumber;
            onDemandName.Focus();
        }

        void GoButtonClick(object sender, EventArgs e)
        {
            if (onDemandName.Text.Trim() == "" || String.Compare(onDemandName.Text.Trim(), "example", StringComparison.OrdinalIgnoreCase) == 0)
            {
                MessageBox.Show("Please enter your on demand URL", "On Demand Name", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                DialogResult = DialogResult.None;
                onDemandName.Focus();
                return;
            }

            // Set the configuration - it will get saved by caller if no exceptions occur.
            configuration.FogBugzOnDemandName = onDemandName.Text;
            configuration.FogBugsLastCaseNumber = (int) caseNumberUpDown.Value;
            IniConfig.Save();

            string fileName = GetFogBugzHtmlFile();

            // Launch the default browser. This file will auto submit to the on demand URL that was set in the template.
            // User needs to login and we don't deal with credentials at all in this plugin.
            Process.Start("file://" + fileName);
        }

        /// <summary>
        /// Creates the HTML file that is used to submit the FogBugz case. This is the same method that the built-in FogBugz Screenshot tool uses.
        /// You'll see that it creates a file when submitting in the temp folder and that file auto submits. 
        /// 
        /// The benefit of this approach (as opposed to using FB API) is that it does not actually create the case until the user clicks OK.
        /// This means you can select the project, priority and so forth - otherwise this must be assigned up front or it chooses defaults, which are often bad.
        /// </summary>
        string GetFogBugzHtmlFile()
        {
            string fileName = Path.Combine(Path.GetTempPath(), "sendToFogbugz.html");
            string html;
            using (Stream resource = Assembly.GetExecutingAssembly().GetManifestResourceStream("GreenshotFogBugzPlugin.submitScreenshot.html"))
            {
                if (resource == null)
                {
                    throw new InvalidOperationException("The sendToFogbugz HTML template could not be found. Was it moved or renamed?");
                }
                using (StreamReader streamReader = new StreamReader(resource, Encoding.UTF8))
                {
                    html = streamReader.ReadToEnd();
                }
            }

            html = html.Replace("{onDemand}", onDemandName.Text);
            html = html.Replace("{newCase}", newCaseButton.Checked ? "1" : "0");
            html = html.Replace("{caseNumber}", existingCaseButton.Checked ? caseNumberUpDown.Value.ToString(CultureInfo.InvariantCulture) : "");
            byte[] buffer = captureStream.ToArray();
            string entireImage = Convert.ToBase64String(buffer);
            int fragmentCount = 0;

            const int maximumFragmentLength = 100000;
            int currentEncodingOffset = 0;

            StringBuilder formBuilder = new StringBuilder();

            int remainder = currentEncodingOffset + maximumFragmentLength > entireImage.Length ? entireImage.Length - currentEncodingOffset : maximumFragmentLength;
            while (remainder > 0)
            {
                fragmentCount++;
                string currentFragment = entireImage.Substring(currentEncodingOffset, remainder);
                formBuilder.Append("<input type=\"hidden\" name=\"base64png");
                formBuilder.Append(fragmentCount);
                formBuilder.Append("\" value=\"");
                formBuilder.Append(currentFragment);
                formBuilder.Append("\">");

                currentEncodingOffset += remainder;
                remainder = currentEncodingOffset + maximumFragmentLength > entireImage.Length ? entireImage.Length - currentEncodingOffset : maximumFragmentLength;
            }
            html = html.Replace("{fragmentCount}", fragmentCount.ToString(CultureInfo.InvariantCulture));
            html = html.Replace("{fragments}", formBuilder.ToString());

            using (StreamWriter streamWriter = new StreamWriter(fileName, false, Encoding.UTF8))
            {
                streamWriter.Write(html);
            }
            return fileName;
        }

        void existingCaseButton_CheckedChanged(object sender, EventArgs e)
        {
            caseNumberUpDown.Enabled = existingCaseButton.Checked;
            UpdateGoButtonLabel();
        }

        void UpdateGoButtonLabel()
        {
            GoButton.Text = existingCaseButton.Checked ? "Update case " + caseNumberUpDown.Value : "Create new case";
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        void caseNumberUpDown_TextChanged(object sender, EventArgs e)
        {
            UpdateGoButtonLabel();
        }
    }
}
