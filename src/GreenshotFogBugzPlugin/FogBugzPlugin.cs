using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using Greenshot.Plugin;
using Greenshot.IniFile;

namespace GreenshotFogBugzPlugin
{
    public class FogBugzPlugin : IGreenshotPlugin
    {
        static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(FogBugzPlugin));
        static FogBugzConfiguration configuration;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            // if (disposing)
            // {
            // Do cleanup here
            // }
        }

        public IEnumerable<IDestination> Destinations()
        {
            yield return new FogBugzDestination(this);
        }

        public IEnumerable<IProcessor> Processors()
        {
            yield break;
        }

        /// <summary>
        /// Called when it's time to initialize the plugin. Not much to do, just grab reference to settings.
        /// </summary>
        /// <returns>true if plugin is initialized, false if not (doesn't show)</returns>
        public virtual bool Initialize(IGreenshotHost pluginHost, PluginAttribute myAttributes)
        {
            configuration = IniConfig.GetIniSection<FogBugzConfiguration>();
            return true;
        }

        public virtual void Shutdown()
        {
            LOG.Debug("FogBugz Plugin shutdown.");
        }

        public virtual void Configure()
        {
            // We decorate in the assembly info that we don't need config. But if we ever get here, show a message;
            MessageBox.Show("Nothing to configure", "Easy!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// This will be called when Greenshot is shutting down
        /// </summary>
        public void Closing(object sender, FormClosingEventArgs e)
        {
            LOG.Debug("Closing called in GreenshotFogBugzPlugin");
            Shutdown();
        }

        /// <summary>
        /// Upload the capture to FogBugz
        /// </summary>
        /// <returns>true if the upload succeeded</returns>
        public bool Upload(ICaptureDetails captureDetails, ISurface surfaceToUpload)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                surfaceToUpload.GetImageForExport().Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                try
                {
                    SendForm sendForm = new SendForm(configuration, stream);
                    return sendForm.ShowDialog() == DialogResult.OK;
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Oops. Something went wrong. Maybe here's a clue: " + exception.Message, "FogBugs Plugin", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return false;
        }
    }
}
