using System.Reflection;
using System.Runtime.InteropServices;
using Greenshot.Plugin;

[assembly: AssemblyTitle("Greenshot plugin for FogBugz")]
[assembly: AssemblyDescription("Submits cases to FogBugz the same way the FogBugz Screenshot app does.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Pieter Venter")]
[assembly: AssemblyProduct("Greenshot plugin for FogBugz")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Since the plugin does not require configuration, set it to false
[assembly: PluginAttribute("GreenshotFogBugzPlugin.FogBugzPlugin", false)] 

// This sets the default COM visibility of types in the assembly to invisible.
// If you need to expose a type to COM, use [ComVisible(true)] on that type.
[assembly: ComVisible(false)]

// The assembly version has following format :
//
// Major.Minor.Build.Revision
//
// You can specify all the values or you can use the default the Revision and 
// Build Numbers by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
