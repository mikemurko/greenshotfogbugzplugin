﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Greenshot.Plugin;
using GreenshotPlugin.Core;

namespace GreenshotFogBugzPlugin
{
    public class FogBugzDestination : AbstractDestination
    {
        readonly FogBugzPlugin plugin;

        public FogBugzDestination(FogBugzPlugin plugin)
        {
            this.plugin = plugin;
        }

        public override Keys EditorShortcutKeys
        {
            get
            {
                return Keys.Control | Keys.F;
            }
        }

        public override string Designation
        {
            get
            {
                return "FogBugz";
            }
        }

        public override string Description
        {
            get
            {
                return "Send to FogBugz";
            }
        }

        public override Image DisplayIcon
        {
            get
            {
                ComponentResourceManager resources = new ComponentResourceManager(typeof(FogBugzPlugin));
                return (Image)resources.GetObject("FogBugz");
            }
        }

        public override ExportInformation ExportCapture(bool manuallyInitiated, ISurface surface, ICaptureDetails captureDetails)
        {
            ExportInformation exportInformation = new ExportInformation(Designation, Description);
            exportInformation.ExportMade = plugin.Upload(captureDetails, surface);
            ProcessExport(exportInformation, surface);
            return exportInformation;
        }
    }
}
